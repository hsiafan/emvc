package test.controller;

import net.dongliu.commons.collection.MapX;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Liu Dong
 */
@Controller
public class TestController {

    @RequestMapping("/test")
    @ResponseBody
    public Object test(@RequestParam int age) {
        return MapX.immutableOf("age", age);
    }
}
