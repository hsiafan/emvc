package test;

import net.dongliu.emvc.HttpServer;
import test.config.TestConfig;

/**
 * @author Liu Dong
 */
public class TestMain {

    public static void main(String[] args) throws Exception {
        new HttpServer()
                .setContextConfigClasses(TestConfig.class)
                .start();
    }
}
