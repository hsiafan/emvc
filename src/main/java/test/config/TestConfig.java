package test.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Liu Dong
 */
@Configuration
@ComponentScan("test.controller")
public class TestConfig {

}
