package net.dongliu.emvc;


import net.dongliu.commons.collection.ListX;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.NCSARequestLog;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.server.handler.RequestLogHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * the main server of apk downloader
 *
 * @author Dong Liu
 */
public class HttpServer {
    private String host = "0.0.0.0";
    private int port = 8080;
    private String resourceDir = "static";
    private List<Class> contextConfigClasses = ListX.immutableOf();

    private String logDir = "logs/";
    private String logName = "access.log";
    private int logRetainDays = 7;

    private int maxThreads = 256;
    private int minThreads = 8;

    private Server server;

    private static Logger logger = LoggerFactory.getLogger(HttpServer.class);

    /**
     * start server
     */
    public HttpServer start() throws Exception {

        QueuedThreadPool pool = new QueuedThreadPool(maxThreads, minThreads);
        pool.setName("jetty-worker");

        server = new Server(pool);
        server.setStopAtShutdown(true);

        // connector
        ServerConnector connector = new ServerConnector(server);
        connector.setHost(host);
        connector.setPort(port);
        connector.setReuseAddress(true);
        server.addConnector(connector);

        // handlers and log
        HandlerCollection handlers = new HandlerCollection();
        ContextHandlerCollection contexts = new ContextHandlerCollection();
        RequestLogHandler requestLogHandler = new RequestLogHandler();
        handlers.setHandlers(new Handler[]{
                contexts,
                getServletContextHandler(getSpringContext()),
                requestLogHandler
        });
        server.setHandler(handlers);

        requestLogHandler.setRequestLog(initRequestLog());

        server.start();
        logger.info("Server started, listened on {}:{}", host, port);
        return this;
    }

    private NCSARequestLog initRequestLog() {
        File logDirFile = new File(logDir);
        if (!logDirFile.exists()) {
            if (!logDirFile.mkdirs()) {
                logger.error("Create request log dir {} failed", logDir);
            }
        }
        NCSARequestLog requestLog = new NCSARequestLog(logDir + logName + ".yyyy_mm_dd");
        requestLog.setFilenameDateFormat("yyyy-MM-dd");
        requestLog.setRetainDays(logRetainDays);
        requestLog.setAppend(true);
        requestLog.setExtended(false);
        requestLog.setLogTimeZone(TimeZone.getDefault().getID());
        requestLog.setLogLocale(Locale.US);
        return requestLog;
    }

    /**
     * wait server to stop
     *
     * @throws InterruptedException
     */
    public HttpServer join() throws InterruptedException {
        server.join();
        return this;
    }

    /**
     * stop sever
     */
    public void stop() throws Exception {
        server.stop();
    }

    // init spring context and dispatch servlet
    private ServletContextHandler getServletContextHandler(WebApplicationContext context) throws IOException {
        ServletContextHandler contextHandler = new ServletContextHandler();
        contextHandler.setErrorHandler(null);
        contextHandler.setContextPath("/");
        contextHandler.addServlet(new ServletHolder(new DispatcherServlet(context)), "/");
        contextHandler.addEventListener(new ContextLoaderListener(context));

        try {
            String realPath = new ClassPathResource(resourceDir).getURI().toString();
            contextHandler.setResourceBase(realPath);
        } catch (FileNotFoundException e) {
            logger.warn("Web resource dir [{}] not exists", resourceDir);
        }
        return contextHandler;
    }

    private WebApplicationContext getSpringContext() {
        AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
        context.register(ContextConfig.class);
        contextConfigClasses.forEach(context::register);
        return context;
    }

    public String getHost() {
        return host;
    }

    /**
     * set host listened on, default 0.0.0.0
     */
    public HttpServer setHost(String host) {
        this.host = host;
        return this;
    }

    public int getPort() {
        return port;
    }

    /**
     * the port listen on, default 8080
     */
    public HttpServer setPort(int port) {
        this.port = port;
        return this;
    }

    public String getLogDir() {
        return logDir;
    }

    /**
     * the path to store request log, default logs/
     */
    public HttpServer setLogDir(String logDir) {
        this.logDir = logDir;
        return this;
    }

    public String getLogName() {
        return logName;
    }

    /**
     * the log file name, default access.log
     */
    public HttpServer setLogName(String logName) {
        this.logName = logName;
        return this;
    }

    public int getLogRetainDays() {
        return logRetainDays;
    }

    /**
     * log file save days. default 7
     */
    public HttpServer setLogRetainDays(int logRetainDays) {
        this.logRetainDays = logRetainDays;
        return this;
    }

    public int getMaxThreads() {
        return maxThreads;
    }

    /**
     * the max worker thread num, default 256
     */
    public HttpServer setMaxThreads(int maxThreads) {
        this.maxThreads = maxThreads;
        return this;
    }

    public int getMinThreads() {
        return minThreads;
    }

    /**
     * the min worker thread num, default 8
     */
    public HttpServer setMinThreads(int minThreads) {
        this.minThreads = minThreads;
        return this;
    }

    public String getResourceDir() {
        return resourceDir;
    }

    /**
     * Set static resource file root path
     */
    public HttpServer setResourceDir(String resourceDir) {
        this.resourceDir = resourceDir;
        return this;
    }

    public List<Class> getContextConfigClasses() {
        return contextConfigClasses;
    }

    /**
     * Spring Annotations driven class package path.
     * Multi path separated by commas, semicolons or whitespace
     */
    public HttpServer setContextConfigClasses(Class... contextConfigClasses) {
        this.contextConfigClasses = ListX.immutableOf(contextConfigClasses);
        return this;
    }
}
