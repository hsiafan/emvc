package net.dongliu.emvc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * catch AbortException and return response
 *
 * @author Dong Liu
 */
public class EmvcHandlerExceptionResolver implements HandlerExceptionResolver {
    private Logger logger = LoggerFactory.getLogger(EmvcHandlerExceptionResolver.class);

    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response,
                                         Object handler, Exception e) {
        return handlePlainException(request, response, e);
    }

    private ModelAndView handlePlainException(HttpServletRequest request,
                                              HttpServletResponse response, Exception e) {
        response.setStatus(500);
        response.setHeader("Content-Type", "text/plain");
        try {
            e.printStackTrace(response.getWriter());
        } catch (IOException ex) {
            logger.error("", ex);
        }
        logger.error("Exception thrown when process request: {}", request.getRequestURI(), e);
        return new ModelAndView();
    }
}
