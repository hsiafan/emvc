package net.dongliu.emvc;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver;

import java.util.Properties;

/**
 * Spring mvc context config
 */
@Configuration
@Import(MvcConfig.class)
public class ContextConfig {
    private static Logger logger = LoggerFactory.getLogger(ContextConfig.class);

    // freemarker config
    @Bean
    FreeMarkerConfigurer getFreeMarkerConfig() {
        // let freemarker use slf4j
        try {
            freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_SLF4J);
        } catch (ClassNotFoundException e) {
            logger.error("Set freemarker to use slf4j failed", e);
        }
        FreeMarkerConfigurer configurer = new FreeMarkerConfigurer();
        // location for freemarker templates
        configurer.setTemplateLoaderPath("classpath:/templates");
        Properties properties = new Properties();
        properties.setProperty("template_exception_handler", FreemarkerExceptionHandler.class.getName());
        configurer.setFreemarkerSettings(properties);
        return configurer;
    }

    /**
     * use freemarker as template viewer
     */
    @Bean
    FreeMarkerViewResolver getViewResolver() {
        FreeMarkerViewResolver viewResolver = new FreeMarkerViewResolver();
        viewResolver.setCache(true);
        viewResolver.setPrefix("/");
        viewResolver.setSuffix(".ftl");
        return viewResolver;
    }

    @Bean(name = "exceptionResolver")
    HandlerExceptionResolver getHandlerExceptionResolver() {
        return new EmvcHandlerExceptionResolver();
    }
}
