package net.dongliu.emvc;

import freemarker.core.Environment;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Writer;

/**
 * @author Dong Liu
 */
public class FreemarkerExceptionHandler implements TemplateExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(FreemarkerExceptionHandler.class);

    @Override
    public void handleTemplateException(TemplateException e, Environment env, Writer out) throws TemplateException {
        // freemarker already log exception, should not log again
        throw e;
    }
}
